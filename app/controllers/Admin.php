<?php

class Admin extends Controller{
    public function index(){
        $data['title'] = "Dashboard";
        $data['username'] = "Admin";
        $this->view('home/index', $data);
    }

    public function kelas(){
     $data['username'] = "Admin";
     $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
     $this->view('admin_kelas/index', $data); 
    }  

     public function tambahDataKelas(){
        if($this->model('Kelas_model')->addDataKelas($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Kelas Berhasil Ditambahkan');
            header('Location: ' . BASE_URL . 'admin/kelas');
           
        } else {
            Flasher::setFlash('danger', 'Data Kelas Gagal Ditambahkan');
            header('Location: ' . BASE_URL . 'admin/kelas');
            exit();
        }
     }

     public function hapusDataKelas($id_kelas)
     {
        if($this->model('Kelas_model')->deleteDataKelas($id_kelas) > 0)
        {
            Flasher::setFlash('success', 'Data Kelas Berhasil Dihapus');
            header('Location: ' . BASE_URL . 'admin/kelas');
            
        } else {
            Flasher::setFlash('danger', 'Data Kelas Gagal Dihapus');
            header('Location: ' . BASE_URL . 'admin/kelas');
            exit();
        }
     }

     public function editDataKelas($id){
        $data['username'] = "Admin";
        $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
        $this->view('admin_kelas/edit', $data); 
     }

     public function prosesEditDataKelas(){
        if($this->model('Kelas_model')->editDataKelas($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Kelas Berhasil Diedit');
            header('Location: ' . BASE_URL . 'admin/kelas');
            
        } else {
            Flasher::setFlash('danger', 'Data Kelas Gagal Diedit');
            header('Location: ' . BASE_URL . 'admin/kelas');
            exit();
        }
     }

     public function pembayaran(){
        $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
        $data['username'] = "Admin";
        $this->view('admin_pembayaran/index', $data);
    }

    public function tambahDataPembayaran(){
        if($this->model('Pembayaran_model')->addDataPembayaran($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Pembayaran Berhasil Ditambahkan');
            header('Location: ' . BASE_URL . 'admin/pembayaran');
            
        } else {
            Flasher::setFlash('danger', 'Data Pembayaran Gagal Ditambahkan');
            header('Location: ' . BASE_URL . 'admin/pembayaran');
            exit();
        }
    }

    public function hapusDataPembayaran($id_pembayaran){
        if($this->model('Pembayaran_model')->deleteDataPembayaran($id_pembayaran) > 0)
        {
            Flasher::setFlash('success', 'Data Pembayaran Berhasil Dihapus');
            header('Location: ' . BASE_URL . 'admin/pembayaran');
            
        } else {
            Flasher::setFlash('danger', 'Data Pembayaran Gagal Dihapus');
            header('Location: ' . BASE_URL . 'admin/pembayaran');
            exit();
        }
        
    }

    public function editDataPembayaran($id_pembayaran){
        $data['username'] = "Admin";
        $data['pembayaran'] = $this->model('Pembayaran_model')->getPembayaranById($id_pembayaran);
        $this->view ('admin_pembayaran/edit', $data);
    }

    public function prosesEditDataPembayaran()
    {
        if($this->model('Pembayaran_model')->editDataPembayaran($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Pembayaran Berhasil Diedit');
            header('Location: ' . BASE_URL . 'admin/pembayaran');
            
        } else {
            Flasher::setFlash('danger', 'Data Pembayaran Gagal Diedit');
            header('Location: ' . BASE_URL . 'admin/pembayaran');
            exit();
        }
    }

     public function petugas(){
        $data['username'] = "Admin";
        $data['petugas'] = $this->model('Petugas_model')->getAllPetugas();
        $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
        $this->view('admin_petugas/index', $data);
    }


    public function tambahDataPetugas(){
        if($this->model('Petugas_model')->addDataPetugas($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Petugas Berhasil Ditambahkan');
            header('Location: ' . BASE_URL . 'admin/petugas');
            
        } else {
            Flasher::setFlash('danger', 'Data Petugas Gagal Ditambahkan');
            header('Location: ' . BASE_URL . 'admin/petugas');
            exit();
        }
    }

    public function hapusDataPetugas($id_petugas){
        if($this->model('Petugas_model')->deleteDataPetugas($id_petugas) > 0)
        {
            Flasher::setFlash('success', 'Data Petugas Berhasil Dihapus');
            header('Location: ' . BASE_URL . 'admin/petugas');
            
        } else {
            Flasher::setFlash('danger', 'Data Petugas Gagal Dihapus');
            header('Location: ' . BASE_URL . 'admin/petugas');
            exit();
        }
    }

    public function editDataPetugas($petugas_id){
        $data['username'] = "Admin";
        $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
        $data['petugas'] = $this->model('Petugas_model')->getPetugasById($petugas_id);
        $this->view('admin_petugas/edit', $data);
    }

    public function prosesEditDataPetugas(){
        if($this->model('Petugas_model')->editDataPetugas($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Petugas Berhasil Diedit');
            header('Location: ' . BASE_URL . 'admin/petugas');
            
        } else {
            Flasher::setFlash('danger', 'Data Petugas Gagal Diedit');
            header('Location: ' . BASE_URL . 'admin/petugas');
            exit();
        }
    }

    public function siswa(){
        $data['username'] = "Admin";
        $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
        $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
        $this->view('admin_siswa/index', $data);
    }

    public function tambahDataSiswa(){
        $data=[
            'username'=> $_POST['nis'],
            'password'=>$_POST['password'],
            'role' => '3'
        ];
        if($this->model('Pengguna_model')->addDataPengguna($data))
        {
            $pengguna = $this->model('Pengguna_model')->getPenggunaIdByUsername($data['username']);
            $data=[
                'nis'=> $_POST['nis'],
                'nisn'=> $_POST['nisn'],
                'nama'=> $_POST['nama'],
                'alamat'=> $_POST['alamat'],
                'telepon'=> $_POST['telepon'],
                'kelas_id'=> $_POST['id_kelas'],
                'pembayaran_id'=> $_POST['id_pembayaran'],
                'pengguna_id'=> $_POST['pengguna_id'],
            ];
            if($this->model('Siswa_model')->addDataSiswa($_POST) > 0)
            {
                Flasher::setFlash('success', 'Data Siswa Berhasil Ditambahkan');
                header('Location: ' . BASE_URL . 'admin/siswa');   
            } else {
                Flasher::setFlash('danger', 'Data Siswa Gagal Ditambahkan');
                header('Location: ' . BASE_URL . 'admin/siswa');
                exit();
            }
        }
        
    }

    public function hapusDataSiswa($id_siswa){
        if($this->model('Siswa_model')->deleteDataSiswa($id_siswa) > 0)
        {
            Flasher::setFlash('success', 'Data Siswa Berhasil Dihapus');
            header('Location: ' . BASE_URL . 'admin/siswa');
            
        } else {
            Flasher::setFlash('danger', 'Data Siswa Gagal Dihapus');
            header('Location: ' . BASE_URL . 'admin/siswa');
            exit();
        }
    }

    public function editDataSiswa($siswa_id){
        $data['username'] = "Admin";
        $data['siswa'] = $this->model('Siswa_model')->getSiswaById($siswa_id);
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
        $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
        $this->view('admin_siswa/edit', $data);
    }

    public function prosesEditDataSiswa(){
        $data=[
            'username' => $_POST['nis'],
            'password' => $_POST['id_pengguna'],
        ];
        if($this->model('Pengguna_model')->updateDataPenggunaModel($data))
        {
            $data=[
                'nis'=> $_POST['nis'],
                'nisn'=> $_POST['nisn'],
                'nama'=> $_POST['nama'],
                'alamat'=> $_POST['alamat'],
                'telepon'=> $_POST['telepon'],
                'id_kelas'=> $_POST['id_kelas'],
                'id_pembayaran'=> $_POST['id_pembayaran'],
                'id_pengguna'=> $_POST['id_pengguna'],
                'id' => $_POST['id'],
            ];
        }
        if($this->model('Siswa_model')->editDataSiswa($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Siswa Berhasil Diedit');
            header('Location: ' . BASE_URL . 'admin/siswa');
            exit;
            
        } else {
            Flasher::setFlash('danger', 'Data Siswa Gagal Diedit');
            header('Location: ' . BASE_URL . 'admin/siswa');
            exit();
        }
    }

    public function transaksi(){
        $data['username'] = "Admin";
        $data = [
            'title' => 'Transaksi',
            'siswa' => $this->model('Siswa_model')->allSiswa()
        ];

        $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
        $this->view('admin_transaksi/index', $data);
    }

    // public function entryPembayaran(){
    //     $data['username'] = "Admin";
    //     $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
    //     $this->view('admin_entrypembayaran/index', $data);
    // }

    // public function detailEntryPembayaran($siswa_id){
    //     $data['username'] = "Admin";
    //     $data['siswa'] = $this->model('Siswa_model')->getAllSiswaById($siswa_id);
    //     $this->view('admin_entrypembayaran/detail', $data);
    // }

     public function entryTransaksi()
    {
        $data = [
            'title' => 'Transaksi',
            'siswa' => $this->model('Siswa_model')->allSiswa()
        ];
     
        $this->view('admin_entrypembayaran/index', $data);
    }

    public function show($id)
    {
        $data = [
            'title' => 'Transaksi Siswa',
            'siswa' => $this->model('Siswa_model')->getDataByID($id),
            'bulan' => $this->model('Transaksi_model')->getBulanByIdTransaksiSiswa($id)
        ];

        $data['dataBulan'] = [
            'Januari' => [
                'Januari', 1
            ],
            'Februari' => [
                'Februari', 2
            ],
            'Maret' => [
                'Maret', 3
            ],
            'April' => [
                'April', 4
            ],
            'Mei' => [
                'Mei', 5
            ],
            'Juni' => [
                'Juni', 6
            ],
            'Juli' => [
                'Juli', 7
            ],
            'Agustus' => [
                'Agustus', 8
            ],
            'September' => [
                'September', 9
            ],
            'Oktober' => [
                'Oktober', 10
            ],
            'November' => [
                'November', 11
            ],
            'Desember' => [
                'Desember', 12
            ],

        ];

        $bulan_dibayar = [];

        foreach ($data['bulan'] as $bulan) {
            array_push($bulan_dibayar, $bulan['bulan_dibayar']);
        }

        $data['bulan_dibayar'] = $bulan_dibayar;

      
        $this->view('admin_entrypembayaran/showTransaksi', $data);
       
    }

    public function storeTransaksi()
    {
        if ($this->model('Transaksi_model')->addTransaksi($_POST) > 0) {
            Flasher::setFlash('success', 'Data Transaksi Berhasil Ditambah');
            header('Location:' . BASE_URL . 'admin/entryTransaksi');
        }
    }

    public function generateLaporan()
    {
        $data = [
            'title' => 'Cetak Laporan',
            'transaksi' => $this->model('Transaksi_model')->allTransaksi(),
        ];

        $data['bulan'] = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'];

        $data['pilihanData'] = [];

        foreach ($data['transaksi'] as $t) {
            $data['pilihanData'][$t['nama'] . '|' . $t['nisn']][] = $t['bulan_dibayar'];
        }

        $this->view('laporan/index', $data);
    }

}