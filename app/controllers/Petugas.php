<?php

class Petugas extends Controller{
    public function index(){
        $data['title'] = "Dashboard";
        $data['username'] = "Petugas";
        $this->view('petugas/index', $data);
    }

    public function petugasKelas(){
        $data['username'] = "Petugas";
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $this->view('petugas_kelas/index', $data);
    }

    public function tambahDataPetugasKelas(){
        if($this->model('Kelas_model')->addDataKelas($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Kelas Berhasil Ditambahkan');
            header('Location: ' . BASE_URL . 'petugas/petugasKelas');
           
        } else {
            Flasher::setFlash('danger', 'Data Kelas Gagal Ditambahkan');
            header('Location: ' . BASE_URL . 'petugas/petugasKelas');
            exit();
        }
     }

      public function hapusDataPetugasKelas($id_kelas)
     {
        if($this->model('Kelas_model')->deleteDataKelas($id_kelas) > 0)
        {
            Flasher::setFlash('success', 'Data Kelas Berhasil Dihapus');
            header('Location: ' . BASE_URL . 'petugas/petugasKelas');
            
        } else {
            Flasher::setFlash('danger', 'Data Kelas Gagal Dihapus');
            header('Location: ' . BASE_URL . 'petugas/petugasKelas');
            exit();
        }
     }

     public function editDataPetugasKelas($id){
        $data['username'] = "Petugas";
        $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
        $this->view('petugas_kelas/edit', $data); 
     }

     public function prosesEditDataPetugasKelas(){
        if($this->model('Kelas_model')->editDataKelas($_POST) > 0)
        {
            Flasher::setFlash('success', 'Data Kelas Berhasil Diedit');
            header('Location: ' . BASE_URL . 'petugas/petugasKelas');
            
        } else {
            Flasher::setFlash('danger', 'Data Kelas Gagal Diedit');
            header('Location: ' . BASE_URL . 'petugas/petugasKelas');
            exit();
        }
     }

     public function petugasTransaksi(){
        $data['username'] = "Petugas";
        $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
        $this->view('petugas_transaksi/index', $data);
    } 

    // public function entryPembayaran(){
    //     $data['username'] = "Petugas";
    //     $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
    //     $this->view('petugas_entrypembayaran/index', $data);
    // }

    // public function detailEntryPembayaran($siswa_id){
    //     $data['username'] = "Petugas";
    //     $data['siswa'] = $this->model('Siswa_model')->getAllSiswaById($siswa_id);
    //     $this->view('petugas_entrypembayaran/detail', $data);
    // }


}