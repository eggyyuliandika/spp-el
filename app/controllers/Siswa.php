<?php

class Siswa extends Controller{
    public function index(){
        $data['username'] = "Siswa";
        $this->view('siswa/index', $data);
    }

    public function siswaTransaksi(){
        $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
         $this->view('siswa_transaksi/index', $data);
    } 

    public function history()
    {
        $id = $_SESSION['id_siswa'];

        $data['history'] = $this->model('Transaksi_model')->getTransaksiByIdSiswa($id);
        $data['title'] = 'History Siswa';

        $this->view('siswa_transaksi/index', $data);
        
    }
}