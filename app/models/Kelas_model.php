<?php

class Kelas_model {
    private $table = 'kelas';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllKelas(){
        $query = "SELECT * FROM {$this->table}";
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function getKelasById($id){
        $query = "SELECT * FROM {$this->table} WHERE id_kelas = :id_kelas";
        $this->db->query($query);
        $this->db->bind("id_kelas", $id);
        $this->db->execute();
        return $this->db->resultSingle();

    }

    public function addDataKelas($data){
        $query = "call tambahDataKelas(:nama, :kompetensi_keahlian)";
        $this->db->query($query);
        $this->db->bind("nama", $data['nama']);
        $this->db->bind("kompetensi_keahlian", $data['kompetensi_keahlian']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteDataKelas($id_kelas){
        $query = "call hapusDataKelas(:id_kelas)";
        $this->db->query($query);
        $this->db->bind("id_kelas", $id_kelas);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function editDataKelas($data){
        $query = "call editDataKelas(:id_kelas, :nama, :kompetensi_keahlian)";
        $this->db->query($query);
        $this->db->bind("id_kelas", $data['id_kelas']);
        $this->db->bind("nama", $data['nama']);
        $this->db->bind("kompetensi_keahlian", $data["kompetensi_keahlian"]);
        $this->db->execute();
        return $this->db->rowCount();  
    }
}