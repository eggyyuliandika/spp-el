<?php

class Pembayaran_model {
    private $table = 'pembayaran';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllPembayaran(){
        $query = "SELECT * FROM {$this->table}";
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function getPembayaranById($id_pembayaran){
        $query = "SELECT * FROM {$this->table} WHERE id_pembayaran = :id_pembayaran";
        $this->db->query($query);
        $this->db->bind("id_pembayaran", $id_pembayaran);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function addDataPembayaran($data){
        $query = "INSERT INTO {$this->table} VALUES(NULL, :tahun_ajaran, :nominal)";
        $this->db->query($query);
        $this->db->bind('tahun_ajaran', $data['tahun_ajaran']);
        $this->db->bind('nominal', $data['nominal']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteDataPembayaran($id_pembayaran){
        $query = 'DELETE FROM ' . $this->table . ' WHERE id_pembayaran = :id_pembayaran';
        $this->db->query($query);
        $this->db->bind('id_pembayaran', $id_pembayaran);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function editDataPembayaran($data){
        $query = 'UPDATE ' . $this->table . ' SET id_pembayaran=:id_pembayaran, tahun_ajaran=:tahun_ajaran, nominal=:nominal WHERE id_pembayaran = :id_pembayaran';
        $this->db->query($query);
        $this->db->bind('id_pembayaran', $data['id_pembayaran']);
        $this->db->bind('tahun_ajaran', $data['tahun_ajaran']);
        $this->db->bind('nominal', $data['nominal']);
        $this->db->execute();
        return $this->db->rowCount(); 
    }

    
}