<?php

class Pengguna_model{
    private $table = 'pengguna';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllPengguna(){
        $query = "SELECT * FROM {$this->table}";
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function getPenggunaByUsername($username){
        $query = "SELECT * FROM {$this->table} WHERE username=:username";
        $this->db->query($query);
        $this->db->bind("username", $username);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function getPenggunaByUsernameAndPassword($data){
        $query = "SELECT * FROM {$this->table} WHERE username=:username AND password=:password";
        $this->db->query($query);
        $this->db->bind("username", $data['username']);
        $this->db->bind("password", $data['password']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getPenggunaIdByUsername($username){
        $query = "SELECT * FROM {$this->table} WHERE username=:username";
        $this->db->query($query);
        $this->db->bind("username", $username);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function addDataPengguna($data){
        $query = "INSERT INTO {$this->table} VALUES (NULL, :username, :password, :role)";
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $data['password']);
        $this->db->bind('role', $data['role']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function updateUsernameById($data){
        $query = "UPDATE {$this->table} SET username = :username WHERE id_pengguna = :id_pengguna";
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('id_pengguna', $data['id_pengguna']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function updateDataPenggunaModel($data){
        $query = "UPDATE {$this->table} SET username = :username WHERE id_pengguna = :id";
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('id', $data['id_pengguna']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteDataPengguna(){
        $query = "DELETE FROM {$this->table} WHERE id_pengguna = id_pengguna";
        $this->db->query($query);
        $this->db->bind('id_pengguna', $_POST['id_pengguna']);
        $this->db->execute();
        return $this->db->rowCount();

    }
}

    

    
