<?php 

class Petugas_model {
    private $table = 'petugas';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllPetugas(){
        $query = "SELECT * FROM {$this->table}";
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function getPetugas($username, $password){
        $query = "SELECT * FROM getpetugas WHERE username = :username AND password = :password";
        $this->db->query($query);
        $this->db->bind('username', $username);
        $this->db->bind('password', $password);
         return $this->db->resultSingle();
    }

    public function getPetugasById($id_petugas){
        $query = "SELECT * FROM {$this->table} WHERE id_petugas = :id_petugas";
        $this->db->query($query);
        $this->db->bind('id_petugas', $id_petugas);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function addDataPetugas($data)
    {
        try {
            $this->db->beginTransaction();
            $query = "INSERT INTO {$this->table} VALUES(NULL, :nama, :pengguna_id)";
            $this->db->query($query);
            $this->db->bind('nama', $data['nama']);
            $this->db->bind('pengguna_id', $data['pengguna_id']);
            $this->db->execute();
            $this->db->commit();
            return $this->db->rowCount();
        } catch (PDOException $e) {
            $this->db->rollback();
            return false;
        }
    }

    public function deleteDataPetugas($id_petugas){
        $query = 'DELETE FROM ' . $this->table . ' WHERE id_petugas = :id_petugas';
        $this->db->query($query);
        $this->db->bind('id_petugas', $id_petugas);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function editDataPetugas($data){
        $query = 'UPDATE ' . $this->table . ' SET id_petugas = :id_petugas, nama=:nama, pengguna_id=:pengguna_id WHERE id_petugas = :id_petugas';
        $this->db->query($query);
        $this->db->bind('id_petugas', $data['id_petugas']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('pengguna_id', $data['pengguna_id']);
        $this->db->execute();
        return $this->db->rowCount();
    }


}