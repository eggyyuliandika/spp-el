<?php

class Siswa_model{
    private $table = 'siswa';
    private $db;

    public function __construct(){
        $this->db = new Database; 
    }

    public function allSiswa(){
        $query = "SELECT * FROM getsiswa";
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function getDataByID($id){
        $query = "SELECT * FROM getsiswa WHERE id_siswa = :id_siswa";
        $this->db->query($query);
        $this->db->bind('id_siswa', $id);
        return $this->db->resultSingle();
    }

    public function getAllSiswa(){
        $query = "SELECT * FROM getallsiswa";
        $this->db->query($query);
        return $this->db->resultAll();
    }

    public function getSiswa($username, $password){
        $query = "SELECT * FROM getsiswa WHERE username = :username AND password = :password";
        $this->db->query($query);
        $this->db->bind('username', $username);
        $this->db->bind('password', $password);
        return $this->db->resultSingle();
    }

    public function getSiswaById($id_siswa){ 
        $query = "SELECT * FROM {$this->table} WHERE id_siswa = :pengguna_id";
        $this->db->query($query);
        $this->db->bind('pengguna_id', $id_siswa);
        return $this->db->resultSingle();
    }

    
    public function getAllSiswaById($id_siswa){ 
        $query = "SELECT * FROM getsiswa WHERE id_siswa = :pengguna_id";
        $this->db->query($query);
        $this->db->bind('pengguna_id', $id_siswa);
        return $this->db->resultSingle();
    }

    public function getSiswaByUsername($data){ 
        $query = "SELECT * FROM {$this->table} WHERE username = :username";
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        return $this->db->resultSingle();
    }


    public function addDataSiswa($data){
        $query = "INSERT INTO {$this->table} VALUES (NULL, :nisn, :nis, :nama, :alamat, :telepon, :kelas_id, :pengguna_id, :pembayaran_id)";
        $this->db->query($query);
        $this->db->bind('nisn', $data['nisn']);
        $this->db->bind('nis', $data['nis']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('telepon', $data['telepon']);
        $this->db->bind('kelas_id', $data['id_kelas']);
        $this->db->bind('pengguna_id', $data['pengguna_id']);
        $this->db->bind('pembayaran_id', $data['id_pembayaran']);
        $this->db->execute();
        return $this->db->rowCount(); 
    }

    public function deleteDataSiswa($id_siswa){
        $query = "DELETE FROM {$this->table} WHERE id_siswa = :id_siswa";
        $this->db->query($query);
        $this->db->bind('id_siswa', $id_siswa);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function editDataSiswa($data){
        $query = "UPDATE {$this->table} SET nisn = :nisn, nis = :nis, nama = :nama, alamat = :alamat, telepon = :telepon, 
                kelas_id = :kelas_id, pengguna_id = :pengguna_id, pembayaran_id = :pembayaran_id WHERE id_siswa = :siswa_id";
        $this->db->query($query);
        $this->db->bind('siswa_id', $data['id_siswa']); 
        $this->db->bind('nisn', $data['nisn']);
        $this->db->bind('nis', $data['nis']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('telepon', $data['telepon']);
        $this->db->bind('kelas_id', $data['id_kelas']);
        $this->db->bind('pengguna_id', $data['pengguna_id']);
        $this->db->bind('pembayaran_id', $data['pembayaran_id']);
        $this->db->execute();
        return $this->db->rowCount(); 

    }
}