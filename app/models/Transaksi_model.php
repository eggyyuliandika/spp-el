<?php

class Transaksi_model {
    private $table = 'transaksi';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllTransaksi(){
        $query = "SELECT * FROM {$this->table}";
        $this->db->query($query);
        return $this->db->resultAll();
    }
     public function allTransaksi()
    {
        $query = "SELECT * FROM gettransaksi";
        $this->db->query($query);
        $this->db->execute();
        return $this->db->resultAll();
        
    }

    public function addTransaksi($data)
    {
        $rawData = $data;

        unset($data['id_siswa']);
        unset($data['id_pembayaran']);
        
        $year = date('Y');

        foreach ($data['bulan_dibayar'] as $d) {
            $query = "INSERT INTO {$this->table} VALUES (NULL, NOW(), :bulan_dibayar, :tahun_dibayar, :siswa_id, :petugas_id, :pembayaran_id)";
            $this->db->query($query);
            $this->db->bind('bulan_dibayar', $d);
            $this->db->bind('tahun_dibayar', "$year");
            $this->db->bind('siswa_id', $rawData['id_siswa']);
            $this->db->bind('petugas_id', 3);
            $this->db->bind('pembayaran_id', $rawData['pembayaran_id']);
            $this->db->execute();
        }

        return $this->db->rowCount();
    }

    public function getBulanByIdTransaksiSiswa($id)
    {
        $query = "SELECT bulan_dibayar FROM transaksi WHERE siswa_id = :siswa_id";
        $this->db->query($query);
        $this->db->bind('siswa_id', $id);
        return $this->db->resultAll();
    }

    public function getTransaksiByIdSiswa($id)
    {
        $query = "SELECT * FROM gettransaksi WHERE siswa_id = :siswa_id";

        $this->db->query($query);
        $this->db->bind('siswa_id', $id);
        return $this->db->resultAll();
    }
};