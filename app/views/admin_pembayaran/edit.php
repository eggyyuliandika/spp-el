<?php require_once TEMPLATE_PATH . '/header.php' ?>
              <div class="container justify-content-center" width = "500" >
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Pembayaran</h5>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>admin/prosesEditDataPembayaran" method="POST">                                   
                       <input type="hidden" class="form-control" id="id_pembayaran" name="id_pembayaran" value="<?=$data['pembayaran']['id_pembayaran']?>">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tahun Ajaran</label>
                        <input type="text" class="form-control" id="tahun_ajaran" name="tahun_ajaran" value="<?=$data['pembayaran']['tahun_ajaran']?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Nominal</label>
                        <input type="text" class="form-control" id="nominal" name="nominal" value="<?=$data['pembayaran']['nominal']?>">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <a href="<?= BASE_URL; ?>admin/pembayaran">
                      <button type="button" class="btn btn-secondary">Batal</button>
                    </a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>          
<?php require_once TEMPLATE_PATH . '/footer.php' ?>
