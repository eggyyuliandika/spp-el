<?php require_once TEMPLATE_PATH . '/header.php' ?>
              <div class="container justify-content-center" width = "500" >
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Petugas</h5>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>admin/prosesEditDataPetugas" method="POST">                                   
                       <input type="hidden" class="form-control" id="id_petugas" name="id_petugas" value="<?=$data['petugas']['id_petugas']?>">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?=$data['petugas']['nama']?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ID Pengguna</label>                                       
                        <input type="text" class="form-control" name="pengguna_id" value="<?=$data['petugas']['pengguna_id']?>">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <a href="<?= BASE_URL; ?>admin/petugas">
                      <button type="button" class="btn btn-secondary">Batal</button>
                    </a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>          
<?php require_once TEMPLATE_PATH . '/footer.php' ?>
