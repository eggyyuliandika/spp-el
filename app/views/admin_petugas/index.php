 <?php require_once TEMPLATE_PATH . '/header.php' ?>
          <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Tabel Data Petugas</h1>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success my-3" data-toggle="modal" data-target="#exampleModal">
              Tambah Data Petugas
            </button>
            <div class = "text-center"><?php  Flasher::flash(); ?></div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Petugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>admin/tambahDataPetugas" method="POST">                                   
                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Pengguna</label>
                        <input type="text" class="form-control" id="pengguna_id" name="pengguna_id" >
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit"  class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>
            </div>

           
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                  Data Petugas
              </div>
              <div class="card-body" style="text-align: center">
                <div class="table-responsive">
                  <table
                    class="table table-bordered"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                  >
                    <thead>
                      <tr>
                        <th>ID Petugas</th>
                        <th>Nama</th>
                        <th>ID Pengguna</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['petugas'] as $petugas):?> 
                      <tr>
                        <td><?=$petugas['id_petugas']?></td>
                        <td><?=$petugas['nama']?></td>
                        <td><?=$petugas['pengguna_id']?></td>
                        <td>
                          <div class="row">
                            <a href="<?= BASE_URL; ?>admin/editDataPetugas/<?=$petugas['id_petugas']?>" class="mt-3 mx-2">
                              <button type="button" class="btn btn-primary">
                                Edit
                              </button>
                            </a>
                            <a href="<?= BASE_URL; ?>admin/hapusDataPetugas/<?=$petugas['id_petugas']?>" class="mt-3 mx-2" onclick="return confirm('yakin?')">
                              <button type="button" class="btn btn-danger">
                                Hapus
                              </button>
                            </a>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        
 <?php require_once TEMPLATE_PATH . '/footer.php' ?>
