<?php require_once TEMPLATE_PATH . '/header.php' ?>
              <div class="container justify-content-center" width = "500" >
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Siswa</h5>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>admin/prosesEditDataSiswa" method="POST">                                   
                       <input type="hidden" class="form-control" id="id_siswa" name="id_siswa" value="<?=$data['siswa']['id_siswa']?>">
                    <div class="form-group">
                        <label for="exampleInputPassword1">NISN</label>
                        <input type="text" class="form-control" id="nisn" name="nisn" value="<?=$data['siswa']['nisn']?>">
                    </div>
                     <div class="form-group">
                        <label for="exampleInputPassword1">NIS</label>
                        <input type="text" class="form-control" id="nis" name="nis" value="<?=$data['siswa']['nis']?>">
                    </div>
                     <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?=$data['siswa']['nama']?>">
                    </div>
                     <div class="form-group">
                        <label for="exampleInputPassword1">Alamat</label>
                        <textarea type="text" class="form-control" id="alamat" name="alamat" value="<?=$data['siswa']['alamat']?>"><?=$data['siswa']['alamat']?></textarea>
                    </div>
                     <div class="form-group">
                        <label for="exampleInputPassword1">Telepon</label>
                        <input type="text" class="form-control" id="telepon" name="telepon" value="<?=$data['siswa']['telepon']?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ID Kelas</label>
                        <select name="id_kelas" class="form-control">
                          <?php foreach($data['kelas'] as $kelas): ?>
                            <option value="<?=$kelas['id_kelas']?>"><?=$kelas['nama']?> <?=$kelas['kompetensi_keahlian']?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ID Pengguna</label>
                        <select name="pengguna_id" class="form-control">
                            <?php foreach($data['pengguna'] as $pengguna): ?>
                                <option value="<?=$pengguna['id_pengguna']?>"><?=$pengguna['role']?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ID Pembayaran</label>
                        <select name="pembayaran_id" class="form-control">
                          <?php foreach($data['pembayaran'] as $pembayaran): ?>
                            <option value="<?=$pembayaran['id_pembayaran']?>"><?=$pembayaran['tahun_ajaran']?> <?=$pembayaran['nominal']?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <a href="<?= BASE_URL; ?>admin/siswa">
                      <button type="button" class="btn btn-secondary">Batal</button>
                    </a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>          
<?php require_once TEMPLATE_PATH . '/footer.php' ?>
