<?php require_once TEMPLATE_PATH . '/header.php'?>
  <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Tabel Data Siswa</h1>
             <!-- Button trigger modal -->
            <button type="button" class="btn btn-success my-3" data-toggle="modal" data-target="#exampleModal">
              Tambah Data Siswa
            </button>
            <div class = "text-center"><?php  Flasher::flash(); ?></div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>admin/tambahDataSiswa" method="POST">                                   
                      <div class="form-group">
                        <label for="exampleInputPassword1">NISN</label>
                        <input type="text" class="form-control" id="nisn" name="nisn">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">NIS</label>
                        <input type="text" class="form-control" id="nis" name="nis">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Alamat</label>
                        <textarea type="text" class="form-control" id="alamat" name="alamat"></textarea>
                      </div>
                       <div class="form-group">
                        <label for="exampleInputPassword1">Telepon</label>
                        <input type="text" class="form-control" id="telepon" name="telepon">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Kelas</label>
                        <select name="id_kelas" class="form-control">
                          <?php foreach($data['kelas'] as $kelas): ?>
                            <option value="<?=$kelas['id_kelas']?>"><?=$kelas['nama']?> <?=$kelas['kompetensi_keahlian']?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Pengguna</label>
                        <select name="pengguna_id" class="form-control">
                          <?php foreach($data['pengguna'] as $pengguna): ?>
                            <option value="<?=$pengguna['id_pengguna']?>"><?=$pengguna['role']?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">ID Pembayaran</label>
                        <select name="id_pembayaran" class="form-control">
                          <?php foreach($data['pembayaran'] as $pembayaran): ?>
                            <option value="<?=$pembayaran['id_pembayaran']?>"><?=$pembayaran['tahun_ajaran']?> <?=$pembayaran['nominal']?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit"  class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>
            </div>

            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                  Data Siswa
              </div>
              <div class="card-body" style="text-align: center">
                <div class="table-responsive">
                  <table
                    class="table table-bordered"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                  >
                    <thead>
                      <tr>
                        <th>ID Siswa</th>
                        <th>NISN</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Kelas</th> 
                        <th>ID Pembayaran</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['siswa'] as $siswa):?> 
                      <tr>
                        <td><?=$siswa['id_siswa']?></td>
                        <td><?=$siswa['nisn']?></td>
                        <td><?=$siswa['nis']?></td>
                        <td><?=$siswa['nama_kelas']?></td>
                        <td><?=$siswa['alamat']?></td>
                        <td><?=$siswa['telepon']?></td>
                        <td><?=$siswa['nama']?> <?=$siswa['kompetensi_keahlian']?></td>
                        <td><?=$siswa['id_pembayaran']?></td>
                        <td>
                          <div class="row">
                            <a href="<?= BASE_URL; ?>admin/editDataSiswa/<?=$siswa['id_siswa']?>" class="mt-3 mx-2">
                              <button type="button" class="btn btn-primary">
                                Edit
                              </button>
                            </a>
                            <a href="<?= BASE_URL; ?>admin/hapusDataSiswa/<?=$siswa['id_siswa']?>" class="mt-3 mx-2" onclick="return confirm('yakin?')">
                              <button type="button" class="btn btn-danger">
                                Hapus
                              </button>
                            </a>
                          </div>          
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
<?php require_once TEMPLATE_PATH . '/footer.php'?>