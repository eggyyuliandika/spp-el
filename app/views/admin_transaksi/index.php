<?php require_once TEMPLATE_PATH . '/header.php'?>
  <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-2 text-gray-800">Tabel History Data Siswa</h1>
                <a
                    href="#"
                    class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
                    ><i class="fas fa-download fa-sm text-white-50"></i> Laporan</a>
            </div>
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                  History Data Transaksi
              </div>
              <div class="card-body" style="text-align: center">
                <div class="table-responsive">
                  <table
                    class="table table-bordered"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                  >
                    <thead>
                      <tr>
                        <th>ID Transasksi</th>
                        <th>Tanggal Bayar</th>
                        <th>Bulan Dibayar</th>
                        <th>Tahun Dibayar</th>
                        <th>ID Siswa</th>
                        <th>ID Petugas</th>
                        <th>ID Pembayaran</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['transaksi'] as $transaksi):?> 
                      <tr>
                        <td><?=$transaksi['id_transaksi']?></td>
                        <td><?=$transaksi['tanggal_bayar']?></td>
                        <td><?=$transaksi['bulan_dibayar']?></td>
                        <td><?=$transaksi['tahun_dibayar']?></td>
                        <td><?=$transaksi['siswa_id']?></td>
                        <td><?=$transaksi['petugas_id']?></td>
                        <td><?=$transaksi['pembayaran_id']?></td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
<?php require_once TEMPLATE_PATH . '/footer.php'?>