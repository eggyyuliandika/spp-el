 <?php require_once TEMPLATE_PATH . '/header_petugas.php' ?>
          <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Tabel Data Pembayaran Siswa</h1>

            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                  Data Pembayaran Siswa
              </div>
              <div class="card-body" style="text-align: center">
                <div class="table-responsive">
                  <table
                    class="table table-bordered"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                  >
                    <thead>
                      <tr>
                        <th>NISN</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Kelas</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['siswa'] as $siswa):?> 
                      <tr>
                        <td><?=$siswa['nisn']?></td>
                        <td><?=$siswa['nis']?></td>
                        <td><?=$siswa['nama_kelas']?></td>
                        <td><?=$siswa['nama']?> <?=$siswa['kompetensi_keahlian']?></td>
                        
                        <td>
                          <div class="row">
                            <a href="<?= BASE_URL; ?>petugas/detailEntryPembayaran/<?=$siswa['id_siswa']?>" class="mt-3 mx-2">
                              <button type="button" class="btn btn-success">
                                Bayar
                              </button>
                            </a>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        
 <?php require_once TEMPLATE_PATH . '/footer.php' ?>
