<?php require_once TEMPLATE_PATH . '/header_petugas.php' ?>
              <div class="container justify-content-center" width = "500" >
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Kelas</h5>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>petugas/prosesEditDataPetugasKelas" method="POST">                                   
                       <input type="hidden" class="form-control" id="id_kelas" name="id_kelas" value="<?=$data['kelas']['id_kelas']?>">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?=$data['kelas']['nama']?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Kompetensi Keahlian</label>
                        <input type="text" class="form-control" id="kompetensi_keahlian" name="kompetensi_keahlian" value="<?=$data['kelas']['kompetensi_keahlian']?>">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <a href="<?= BASE_URL; ?>petugas/petugasKelas">
                      <button type="button" class="btn btn-secondary">Batal</button>
                    </a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>          
<?php require_once TEMPLATE_PATH . '/footer.php' ?>
