 <?php require_once TEMPLATE_PATH . '/header_petugas.php' ?>
          <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Tabel Data Kelas</h1>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success my-3" data-toggle="modal" data-target="#exampleModal">
              Tambah Data Kelas
            </button>
            <div class = "text-center"><?php  Flasher::flash(); ?></div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form class="user" action="<?= BASE_URL; ?>petugas/tambahDataPetugasKelas" method="POST">                                   
                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Kompetensi Keahlian</label>
                        <input type="text" class="form-control" id="kompetensi_keahlian" name="kompetensi_keahlian">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit"  class="btn btn-primary">Simpan</button>
                  </div>
                   </form>
                </div>
              </div>
            </div>
           
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                  Data Kelas
              </div>
              <div class="card-body"  style="text-align: center">
                <div class="table-responsive">
                  <table
                    class="table table-bordered"
                    id="dataTable"
                    width="100%"
                    cellspacing="0"
                  >
                    <thead>
                      <tr>
                        <th>ID Kelas</th>
                        <th>Nama</th>
                        <th>Kompetensi Keahlian</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['kelas'] as $kelas):?> 
                      <tr>
                        <td><?=$kelas['id_kelas']?></td>
                        <td><?=$kelas['nama']?></td>
                        <td><?=$kelas['kompetensi_keahlian']?></td>
                        <td>
                          <div class="row">
                            <a href="<?= BASE_URL; ?>petugas/editDataPetugasKelas/<?=$kelas['id_kelas']?>" class="mt-3 mx-2">
                              <button type="button" class="btn btn-primary">
                                Edit
                              </button>
                            </a>
                            <a href="<?= BASE_URL; ?>petugas/hapusDataPetugasKelas/<?=$kelas['id_kelas']?>" class="mt-3 mx-2" onclick="return confirm('yakin?')">
                              <button type="button" class="btn btn-danger">
                                Hapus
                              </button>
                            </a>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        
 <?php require_once TEMPLATE_PATH . '/footer.php' ?>
