 <!-- Sidebar -->
      <ul
        class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
        id="accordionSidebar"
      >
        <!-- Sidebar - Brand -->
        <a
          class="sidebar-brand d-flex align-items-center justify-content-center"
          href="index.html"
        >
          <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
          </div>
          <div class="sidebar-brand-text mx-3">SPP</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0" />

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a
          >
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Heading -->
        <!-- <div class="sidebar-heading">Interface</div> -->

        <!-- Nav Item - Pages Collapse Menu -->
         <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>admin/siswa">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Siswa</span></a
          >
        </li>

        <!-- Nav Item - Utilities Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>admin/petugas">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Petugas</span></a
          >
        </li>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>admin/kelas">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Kelas</span>
            </a
          >
        </li>

        <!-- Nav Item - Pages Collapse Menu -->

        <!-- Nav Item - Charts -->
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>admin/pembayaran">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Pembayaran</span></a
          >
        </li>

         <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>admin/entryTransaksi">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Entry Pembayaran</span></a
          >
        </li>

         <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>admin/generateLaporan">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Generate Laporan</span></a
          >
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block" />

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

        <!-- Sidebar Message -->
        <!-- <div class="sidebar-card d-none d-lg-flex">
          <img
            class="sidebar-card-illustration mb-2"
            src="img/undraw_rocket.svg"
            alt="..."
          />
          <p class="text-center mb-2">
            <strong>SB Admin Pro</strong> is packed with premium features,
            components, and more!
          </p>
          <a
            class="btn btn-success btn-sm"
            href="https://startbootstrap.com/theme/sb-admin-pro"
            >Upgrade to Pro!</a
          >
        </div> -->
      </ul>
      <!-- End of Sidebar -->