-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 13, 2023 at 02:39 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spp`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `editDataKelas` (IN `id` INT(11), IN `ed_nama` VARCHAR(10), IN `ed_kompetensi_keahlian` VARCHAR(50))   BEGIN
UPDATE kelas
set
nama = ed_nama, kompetensi_keahlian = ed_kompetensi_keahlian WHERE id_kelas = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `hapusDataKelas` (IN `id` INT)   BEGIN
delete from kelas where id_kelas = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `tambahDataKelas` (IN `nama` VARCHAR(10), IN `kompetensi_keahlian` VARCHAR(50))   BEGIN
insert into kelas VALUES (NULL, nama, kompetensi_keahlian);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `getallsiswa`
-- (See below for the actual view)
--
CREATE TABLE `getallsiswa` (
`id_siswa` int
,`nisn` varchar(10)
,`nis` varchar(5)
,`nama_kelas` varchar(50)
,`alamat` text
,`telepon` varchar(14)
,`id_kelas` int
,`nama` varchar(10)
,`kompetensi_keahlian` varchar(50)
,`id_pembayaran` int
,`tahun_ajaran` varchar(9)
,`nominal` int
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `getpetugas`
-- (See below for the actual view)
--
CREATE TABLE `getpetugas` (
`id_petugas` int
,`nama` varchar(50)
,`pengguna_id` int
,`username` varchar(25)
,`password` varchar(128)
,`role` int
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `getsiswa`
-- (See below for the actual view)
--
CREATE TABLE `getsiswa` (
`id_siswa` int
,`nisn` varchar(10)
,`nis` varchar(5)
,`nama` varchar(50)
,`alamat` text
,`telepon` varchar(14)
,`kelas_id` int
,`pengguna_id` int
,`pembayaran_id` int
,`username` varchar(25)
,`password` varchar(128)
,`role` int
,`tahun_ajaran` varchar(9)
,`nominal` int
,`nama_kelas` varchar(10)
,`kompetensi_keahlian` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `gettransaksi`
-- (See below for the actual view)
--
CREATE TABLE `gettransaksi` (
`id_transaksi` int
,`tanggal_bayar` datetime
,`bulan_dibayar` int
,`tahun_dibayar` int
,`siswa_id` int
,`petugas_id` int
,`pembayaran_id` int
,`nisn` varchar(10)
,`nis` varchar(5)
,`nama` varchar(50)
,`alamat` text
,`telepon` varchar(14)
,`tahun_ajaran` varchar(9)
,`nominal` int
);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int NOT NULL,
  `nama` varchar(10) NOT NULL,
  `kompetensi_keahlian` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama`, `kompetensi_keahlian`) VALUES
(4, 'X - 1', 'MM'),
(5, 'XI - 2', 'MM'),
(6, 'XII - 3', 'MM'),
(23, 'X - 1', 'TKJ'),
(24, 'XI - 2', 'TKJ'),
(25, 'XII - 3', 'TKJ'),
(26, 'XII - 3', 'TPTU'),
(27, 'XII - 3', 'TPTU'),
(28, 'XII - 3', 'TPTU'),
(29, 'XI - 2', 'PM');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int NOT NULL,
  `tahun_ajaran` varchar(9) NOT NULL,
  `nominal` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `tahun_ajaran`, `nominal`) VALUES
(1, '2020/2021', 1000000),
(2, '2021/2022', 1000000),
(3, '2022/2023', 1200000);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int NOT NULL,
  `username` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `username`, `password`, `role`) VALUES
(1, 'admin', 'admin', 1),
(2, 'petugas', 'petugas', 2),
(3, 'siswa', 'siswa', 3),
(41, '28900', 'eggy', 3),
(42, '123', 'tes', 3),
(43, '28921', 'nadiaaja', 3);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int NOT NULL,
  `nama` varchar(50) NOT NULL,
  `pengguna_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama`, `pengguna_id`) VALUES
(3, 'petugas 1', 1),
(4, 'petugas 2', 1),
(8, 'admin1', 1),
(9, 'admin 2', 1),
(10, 'admin 4', 1),
(12, 'admin 3', 1),
(15, 'admin 5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `nis` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(14) NOT NULL,
  `kelas_id` int NOT NULL,
  `pengguna_id` int NOT NULL,
  `pembayaran_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nisn`, `nis`, `nama`, `alamat`, `telepon`, `kelas_id`, `pengguna_id`, `pembayaran_id`) VALUES
(14, '0053272575', '28900', 'Eggy', 'TES', '123', 4, 41, 1),
(16, '0053272522', '28921', 'Putu Nadia', 'Jln. Cokroaminoto No. 1', '081567345123', 29, 43, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `bulan_dibayar` int NOT NULL,
  `tahun_dibayar` int NOT NULL,
  `siswa_id` int NOT NULL,
  `petugas_id` int NOT NULL,
  `pembayaran_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tanggal_bayar`, `bulan_dibayar`, `tahun_dibayar`, `siswa_id`, `petugas_id`, `pembayaran_id`) VALUES
(13, '2023-03-10 13:19:57', 4, 2023, 14, 3, 1),
(14, '2023-03-10 13:19:57', 7, 2023, 14, 3, 1),
(15, '2023-03-10 13:20:37', 4, 2023, 14, 3, 1),
(16, '2023-03-10 13:20:37', 7, 2023, 14, 3, 1),
(17, '2023-03-10 13:23:29', 4, 2023, 14, 3, 1),
(18, '2023-03-10 13:23:29', 7, 2023, 14, 3, 1),
(19, '2023-03-10 13:25:36', 4, 2023, 14, 3, 1),
(20, '2023-03-10 13:25:36', 7, 2023, 14, 3, 1),
(21, '2023-03-10 13:29:52', 2, 2023, 14, 3, 1),
(22, '2023-03-10 13:29:52', 5, 2023, 14, 3, 1),
(23, '2023-03-10 13:34:05', 2, 2023, 14, 3, 1),
(24, '2023-03-10 13:34:05', 5, 2023, 14, 3, 1),
(25, '2023-03-10 13:34:12', 12, 2023, 14, 3, 1),
(26, '2023-03-10 13:36:13', 8, 2023, 14, 3, 1),
(27, '2023-03-10 13:36:13', 11, 2023, 14, 3, 1),
(28, '2023-03-10 13:37:47', 1, 2023, 14, 3, 1),
(29, '2023-03-10 13:50:55', 1, 2023, 16, 3, 1),
(30, '2023-03-10 13:50:55', 2, 2023, 16, 3, 1),
(31, '2023-03-10 13:50:55', 3, 2023, 16, 3, 1);

-- --------------------------------------------------------

--
-- Structure for view `getallsiswa`
--
DROP TABLE IF EXISTS `getallsiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getallsiswa`  AS SELECT `siswa`.`id_siswa` AS `id_siswa`, `siswa`.`nisn` AS `nisn`, `siswa`.`nis` AS `nis`, `siswa`.`nama` AS `nama_kelas`, `siswa`.`alamat` AS `alamat`, `siswa`.`telepon` AS `telepon`, `kelas`.`id_kelas` AS `id_kelas`, `kelas`.`nama` AS `nama`, `kelas`.`kompetensi_keahlian` AS `kompetensi_keahlian`, `pembayaran`.`id_pembayaran` AS `id_pembayaran`, `pembayaran`.`tahun_ajaran` AS `tahun_ajaran`, `pembayaran`.`nominal` AS `nominal` FROM ((`siswa` left join `kelas` on((`siswa`.`kelas_id` = `kelas`.`id_kelas`))) left join `pembayaran` on((`siswa`.`pembayaran_id` = `pembayaran`.`id_pembayaran`)))  ;

-- --------------------------------------------------------

--
-- Structure for view `getpetugas`
--
DROP TABLE IF EXISTS `getpetugas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getpetugas`  AS SELECT `petugas`.`id_petugas` AS `id_petugas`, `petugas`.`nama` AS `nama`, `petugas`.`pengguna_id` AS `pengguna_id`, `pengguna`.`username` AS `username`, `pengguna`.`password` AS `password`, `pengguna`.`role` AS `role` FROM (`petugas` join `pengguna` on((`petugas`.`pengguna_id` = `pengguna`.`id_pengguna`)))  ;

-- --------------------------------------------------------

--
-- Structure for view `getsiswa`
--
DROP TABLE IF EXISTS `getsiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getsiswa`  AS SELECT `siswa`.`id_siswa` AS `id_siswa`, `siswa`.`nisn` AS `nisn`, `siswa`.`nis` AS `nis`, `siswa`.`nama` AS `nama`, `siswa`.`alamat` AS `alamat`, `siswa`.`telepon` AS `telepon`, `siswa`.`kelas_id` AS `kelas_id`, `siswa`.`pengguna_id` AS `pengguna_id`, `siswa`.`pembayaran_id` AS `pembayaran_id`, `pengguna`.`username` AS `username`, `pengguna`.`password` AS `password`, `pengguna`.`role` AS `role`, `pembayaran`.`tahun_ajaran` AS `tahun_ajaran`, `pembayaran`.`nominal` AS `nominal`, `kelas`.`nama` AS `nama_kelas`, `kelas`.`kompetensi_keahlian` AS `kompetensi_keahlian` FROM (((`siswa` join `pengguna` on((`siswa`.`pengguna_id` = `pengguna`.`id_pengguna`))) join `pembayaran` on((`siswa`.`pembayaran_id` = `pembayaran`.`id_pembayaran`))) join `kelas` on((`siswa`.`kelas_id` = `kelas`.`id_kelas`)))  ;

-- --------------------------------------------------------

--
-- Structure for view `gettransaksi`
--
DROP TABLE IF EXISTS `gettransaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gettransaksi`  AS SELECT `transaksi`.`id_transaksi` AS `id_transaksi`, `transaksi`.`tanggal_bayar` AS `tanggal_bayar`, `transaksi`.`bulan_dibayar` AS `bulan_dibayar`, `transaksi`.`tahun_dibayar` AS `tahun_dibayar`, `transaksi`.`siswa_id` AS `siswa_id`, `transaksi`.`petugas_id` AS `petugas_id`, `transaksi`.`pembayaran_id` AS `pembayaran_id`, `siswa`.`nisn` AS `nisn`, `siswa`.`nis` AS `nis`, `siswa`.`nama` AS `nama`, `siswa`.`alamat` AS `alamat`, `siswa`.`telepon` AS `telepon`, `pembayaran`.`tahun_ajaran` AS `tahun_ajaran`, `pembayaran`.`nominal` AS `nominal` FROM ((`transaksi` join `siswa` on((`transaksi`.`siswa_id` = `siswa`.`id_siswa`))) join `pembayaran` on((`transaksi`.`pembayaran_id` = `pembayaran`.`id_pembayaran`)))  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `pengguna_id` (`pengguna_id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `kelas_id` (`kelas_id`,`pengguna_id`,`pembayaran_id`),
  ADD KEY `siswa_ibfk_1` (`pembayaran_id`),
  ADD KEY `siswa_ibfk_3` (`pengguna_id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `siswa_id` (`siswa_id`,`pembayaran_id`),
  ADD KEY `petugas_id` (`petugas_id`) USING BTREE,
  ADD KEY `transaksi_ibfk_3` (`pembayaran_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`pembayaran_id`) REFERENCES `pembayaran` (`id_pembayaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_ibfk_3` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`siswa_id`) REFERENCES `siswa` (`id_siswa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`pembayaran_id`) REFERENCES `pembayaran` (`id_pembayaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_4` FOREIGN KEY (`petugas_id`) REFERENCES `petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
